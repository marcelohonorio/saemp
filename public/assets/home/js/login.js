(function($) {
  const Login = {
    inputs: {},

  start: function() {
    this.$btnLogin     = $('[data-btn-login]');
    this.$modal        = $('[data-login-modal]');

    this.form             = this.$modal.find('[data-login-form]');
    this.btnLoginConfirm  = this.$modal.find('[data-btn-confirmLogin]');
    this.alert            = this.$modal.find('[data-confirmation-alert-danger]');

    this.bind();
  },

  bind: function(){
    this.$btnLogin.on('click', $.proxy(this.onBtnLoginClick, this));
    this.btnLoginConfirm.on('click', $.proxy(this.onBtnConfirmLoginClick, this));
  },

  onBtnLoginClick: function(event) {
    event.preventDefault();
    this.$modal.modal('show');
  },

  onBtnConfirmLoginClick: function(event) {
    event.preventDefault();
    this.alert.removeClass('sr-only');
  }


  };
  $(function() {
    Login.start();
  });
})(jQuery);