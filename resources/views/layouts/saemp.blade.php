<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="author" content="Marcelo">
    
    <title>Saemp - Sistema de Avaliação de Empresas de Itajubá</title>    

    {{-- Favicon --}}
    <link rel="shortcut icon" href="{{ asset('assets/home/img/favicon.png') }}">
    
    {{-- Bootstrap CSS --}}
    <link rel="stylesheet" href="{{ asset('assets/home/css/bootstrap.min.css')}}" type="text/css">    
    <link rel="stylesheet" href="{{ asset('assets/home/css/jasny-bootstrap.min.css')}}" type="text/css">  
    <link rel="stylesheet" href="{{ asset('assets/home/css/bootstrap-select.min.css')}}" type="text/css">  
        
    {{-- Material CSS --}}
    <link rel="stylesheet" href="{{ asset('assets/home/css/material-kit.css')}}" type="text/css">
        
    {{-- Font Awesome CSS --}}
    <link rel="stylesheet" href="{{ asset('assets/home/fonts/font-awesome.min.css')}}" type="text/css"> 
    <link rel="stylesheet" href="{{ asset('assets/home/fonts/themify-icons.css')}}"> 
    
    {{-- Animate CSS --}}
    <link rel="stylesheet" href="{{ asset('assets/home/extras/animate.css')}}" type="text/css">
        
    {{-- Owl Carousel --}}
    <link rel="stylesheet" href="{{ asset('assets/home/extras/owl.carousel.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/home/extras/owl.theme.css')}}" type="text/css">
        
    {{-- Rev Slider CSS --}}
    <link rel="stylesheet" href="{{ asset('assets/home/extras/settings.css')}}" type="text/css"> 
        
    {{-- Slicknav js --}}
    <link rel="stylesheet" href="{{ asset('assets/home/css/slicknav.css')}}" type="text/css">
        
    {{-- Main Styles --}}
    <link rel="stylesheet" href="{{ asset('assets/home/css/main.css')}}" type="text/css">
        
    {{-- Responsive CSS Styles --}}
    <link rel="stylesheet" href="{{ asset('assets/home/css/responsive.css')}}" type="text/css">
    
    {{-- Color CSS Styles --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/home/css/colors/red.css')}}" media="screen" />
    
  </head>

  <body>  

    @yield('content')

    @include('partials.login.modal')

    {{-- Main JS  --}}
    <script type="text/javascript" src="{{ asset('assets/home/js/jquery-min.js')}}"></script>      
    <script type="text/javascript" src="{{ asset('assets/home/js/login.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/home/js/bootstrap.min.js')}}"></script>    
    <script type="text/javascript" src="{{ asset('assets/home/js/material.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/home/js/material-kit.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/home/js/jquery.parallax.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/home/js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/home/js/jquery.slicknav.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/home/js/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/home/js/jquery.counterup.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/home/js/waypoints.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/home/js/jasny-bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/home/js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/home/js/form-validator.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/home/js/contact-form-script.js')}}"></script>    
        
  </body>
</html>