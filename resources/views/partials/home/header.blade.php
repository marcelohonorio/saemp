{{-- Header Sction Start --}}
<div class="header">
  {{-- Strat intro section --}}
  <section id="intro" class="section-intro">
    <div class="logo-menu">
      @include('partials.home.menu')
    {{-- Off Canvas Navigation --}}
    <div class="navmenu navmenu-default navmenu-fixed-left offcanvas"> 
    {{-- Off Canvas Side Menu --}}
      <div class="close" data-toggle="offcanvas" data-target=".navmenu">
          <i class="ti-close"></i>
      </div>
</div>
{{-- Header Section End --}}
<div class="search-container">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h1>Pesquise um estabelecimento</h1><br><h2>Mais de <strong>12.000</strong> empresas cadastradas em nosso banco de dados!</h2>
              <div class="content">
                <form method="" action="">
                  <div class="row">
                    <div class="col-md-8 col-sm-6">
                      <div class="form-group">
                        <input class="form-control" type="text" placeholder="produto ou serviço / palavras-chave / nome da empresa">
                        <i class="ti-direction"></i>
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                      <div class="search-category-container">
                        <label class="styled-select">
                          <select class="dropdown-product selectpicker">
                            <option>Todas as categorias</option>
                            <option>Bares</option>
                            <option>Restaurantes</option>
                            <option>Lanchonetes</option>
                            <option>Pizzarias</option>
                            <option>Sale/Markting</option>
                            <option>Healthcare</option>
                            <option>Science</option>                              
                            <option>Food Services</option>
                          </select>
                        </label>
                      </div>
                    </div>
                    <div class="col-md-1 col-sm-6">
                      <button type="button" class="btn btn-search-icon"><i class="ti-search"></i></button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="popular-jobs">
                <b>Palavras-chave polulares: </b>
                <a href="#">Sushi</a>
                <a href="#">Loja de roupas</a>
                <a href="#">Gasolina</a>
                <a href="#">Curso de inglês</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    {{-- end intro section --}}
    </div>