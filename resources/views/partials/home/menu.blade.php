<nav class="navbar navbar-default" role="navigation" data-spy="affix" data-offset-top="50">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand logo" href="index.html"><img src="{{ asset('assets/home/img/logo.png')}}" alt=""></a>
      </div>
      <div class="collapse navbar-collapse" id="navbar">              
      @include('partials.home.navbar')
      <ul class="nav navbar-nav navbar-right float-right">
        <li class="right"><a data-btn-login><i class="ti-lock"></i>  Login</a></li>
        <li class="right"><a href="" data-btn-signup><i class="ti-rocket"></i>Cadastre-se</a></li>
      </ul>
    </div>                           
  </div>
@include('partials.home.mobile-navbar')
</nav>