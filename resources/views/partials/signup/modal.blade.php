<div class="modal fade" tabindex="-1" role="dialog" data-login-modal>
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
          Login
        </h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-success alert-dismissible fade show sr-only" data-confirmation-alert>
          Enviado! Por favor, verifique também no SPAM.
        </div>

        <div class="alert alert-danger show sr-only" data-confirmation-alert-danger>
          Dados incorretos.
        </div>

        <form data-login-form>
          <div class="form-group">
            <label for="customer-email" class="sr-only">
              E-mail
            </label>
            <input
              id="customer-email"
              class="form-control"
              name="customer-email"
              placeholder="E-mail"
              data-input-email
              type="email"
            >
          </div>
          <div class="form-group">
            <label for="customer-password" class="sr-only">
              Senha
            </label>
            <input
              id="customer-password"
              class="form-control"
              name="customer-password"
              placeholder="Senha"
              data-input-password
              type="password"
            >
          </div>
          <p><a href="#">Esqueci minha senha</a></p>         
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-btn-confirmLogin>
          Login
        </button>
      </div>
    </div>
  </div>
</div>
