<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValuationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('valuations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customers_id')->unsigned();
            $table->integer('companies_id')->unsigned();
            $table->integer('items_id')->unsigned();
            $table->integer('grade')->insigned()->default(0);
            $table->string('comment')->nullable();

            $table->foreign('customers_id')
                ->references('id')
                ->on('customers')
                ->onDelete('cascade');
            
            $table->foreign('companies_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');
            
            $table->foreign('items_id')
                ->references('id')
                ->on('company_evaluations')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valuations');
    }
}
