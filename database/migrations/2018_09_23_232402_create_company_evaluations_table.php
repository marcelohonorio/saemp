<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('companies_id')->unsigned();
            $table->integer('items_id')->unsigned();

            $table->foreign('companies_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');
            
            $table->foreign('items_id')
                ->references('id')
                ->on('evaluation_items')
                ->onDelete('cascade');
                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_evaluations');
    }
}
